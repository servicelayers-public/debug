# Debug docker container

Contains a lot of debugging tools for production. Based on alpine.

* Build/code: `build-base git go bash bash-completion ncurses vim tmux`
* Network: `bind-tools iputils tcpdump curl nmap tcpflow iftop net-tools mtr netcat-openbsd bridge-utils iperf ngrep tcptraceroute nmap-scripts`
* Certificates: `ca-certificates openssl`
* Processes/IO: `htop atop strace iotop dstat sysstat ltrace ncdu logrotate hdparm pciutils psmisc tree pv`

## Use tmux inside tmux

Just double press `<ctrl>+b` to use the inner tmux

# Deploy into Kubernetes Cluster

```
helm install https://servicelayers-public.gitlab.io/debug/debug-0.0.2.tgz --generate-name
```

# References

* [original](https://github.com/giantswarm/debug)
